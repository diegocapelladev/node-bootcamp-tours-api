const dotenv = require('dotenv')
const mongoose = require('mongoose')

dotenv.config()

async function main() {
  await mongoose
    .connect(process.env.MONGO_URL)
    .then(() => console.log('Database Connected!'))
    .catch((err) => {
      console.log(err)
    })
}

main()

module.exports = mongoose
